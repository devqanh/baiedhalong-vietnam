<div class="tcb-clear" data-css="tve-u-15ddcd9721f"><div class="thrv_wrapper thrv_icon tcb-icon-display tve_evt_manager_listen tve_et_click tve_ea_close_lightbox" data-css="tve-u-15ddcd90a30" style="" data-tcb-events="__TCB_EVENT_[{&quot;config&quot;:{},&quot;a&quot;:&quot;close_lightbox&quot;,&quot;t&quot;:&quot;click&quot;}]_TNEVE_BCT__"><svg class="tcb-icon" viewBox="0 0 30 32" data-name="closeclose">
<title>close</title>
<path d="M0.655 2.801l1.257-1.257 27.655 27.655-1.257 1.257-27.655-27.655z"></path>
<path d="M28.31 1.543l1.257 1.257-27.655 27.655-1.257-1.257 27.655-27.655z"></path>
</svg></div></div><div class="thrv_wrapper thrv_text_element tve_empty_dropzone" style="" data-css="tve-u-15ddcd09169"><p data-css="tve-u-15ddccfe1a2" data-default="Enter your text here..." style="text-align: center;"><strong>Request a Consultation to Find Your Dream Home</strong></p></div><div class="thrv_wrapper thrv_text_element tve_empty_dropzone" style="" data-css="tve-u-15ddcd1560f"><p data-css="tve-u-15ddcd11c82" data-default="Enter your text here..." style="text-align: center;">We’ll help you find the right house that suits your lifestyle and budget.<s>​</s></p></div><div class="thrv_wrapper thrv_lead_generation" data-connection="api" style="" data-css="tve-u-15ddcd199aa"><input type="hidden" class="tve-lg-err-msg" value="{&quot;email&quot;:&quot;Email address invalid&quot;,&quot;phone&quot;:&quot;Phone number invalid&quot;,&quot;password&quot;:&quot;Password invalid&quot;,&quot;passwordmismatch&quot;:&quot;Password mismatch error&quot;,&quot;required&quot;:&quot;Required field missing&quot;}">
<div class="thrv_lead_generation_container tve_clearfix">
<form action="#" method="post" novalidate="novalidate">
<div class="tve_lead_generated_inputs_container tve_clearfix tve_empty_dropzone">
<div class="tve_lg_input_container tve_lg_input" data-css="tve-u-15ddcd25fce">
<input type="text" data-field="name" name="name" placeholder="Name" data-placeholder="Name" data-required="0" data-validation="" class="" style="cursor: auto; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII=&quot;);">
</div><div class="tve_lg_input_container tve_lg_input" data-css="tve-u-15ddcd25fd7">
<input type="email" data-field="email" data-required="1" data-validation="email" name="email" placeholder="Email" data-placeholder="Email" class="" style="">
</div>
<div class="tve_lg_input_container tve_lg_input" data-css="tve-u-15ddcd25fde">
<input type="text" data-field="phone" data-required="0" data-validation="" name="phone" placeholder="Phone" data-placeholder="Phone" class="" style="">
</div><div class="tve_lg_input_container tve_submit_container tve_lg_submit" data-css="tve-u-15ddcd50cda">
<button type="submit" style=""><span>Get Started Now</span></button>
</div>
</div>
<input type="hidden" name="__tcb_lg_fc" id="__tcb_lg_fc" value="YTowOnt9"><input id="_submit_option" type="hidden" name="_submit_option" value="reload"><input id="_back_url" type="hidden" name="_back_url" value=""></form>
</div>
</div>